import React, { useState } from 'react'
import AdderPlumbing from './adder_plumbing.js'

const AdderPromise = AdderPlumbing({
	noInitialRun: true,
	noExitRuntime: true
})

const AddNumbers = () => {
	const [ adderModule, setAdderModule] = useState()

	AdderPromise.then ( mod => {
		setAdderModule(mod)
		console.log(mod._adder(1,2))
	})


	return (
		<h1>Hello 1+2= {adderModule && adderModule._adder(1,2)}</h1>
	)
}

export default AddNumbers
